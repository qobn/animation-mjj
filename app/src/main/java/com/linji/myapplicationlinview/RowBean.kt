package com.linji.myapplicationlinview

class RowBean {
    var rawX:Float=0f
    var destX:Float=0f
    var currentX:Float = 0f
    var middleX:Float = 0f
    var moveDistance = 150
    var airDistance = 0f
    var name:String = "0"
    override fun toString(): String {
        return "RowBean(rawX=$rawX, destX=$destX, currentX=$currentX, middleX=$middleX, moveDistance=$moveDistance, name='$name')"
    }
}