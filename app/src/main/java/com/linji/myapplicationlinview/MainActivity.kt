package com.linji.myapplicationlinview

import android.animation.Animator
import android.animation.Animator.AnimatorListener
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.view.ViewTreeObserver
import android.widget.FrameLayout
import android.widget.HorizontalScrollView
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.get
import kotlin.random.Random


class MainActivity : AppCompatActivity() {
    var scroll_View:HorizontalScrollView?=null
    var ll_out:LinearLayout?=null
    var tv_set_ll_out_width:TextView?=null
    var ll_out2:LinearLayout?=null
    var tv_move_right:TextView?=null
    var tv_move_left:TextView?=null
    var tv_set_left:TextView?=null
    var tv_set_middle:TextView?=null
    var tv_set_right:TextView?=null
    var tv_air:TextView?=null
    var tv_megre:TextView?=null
    var tv_stop:TextView?=null
    var tv_go_on:TextView?=null
    var tv_move_middle:TextView?=null
    var fixRow = 0
    var selectIndex = 0
    var allRows = 40
    var allBeans:ArrayList<RowBean> = ArrayList()
    var moveTime:Long = 200
    var allAnimation :ArrayList<ValueAnimator?> = ArrayList()
    var isAir = false
    var delayAddTime = 1000

    var original_width = 0
    var ventilation_width = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        allBeans.clear()
        ventilation_width = 0
        scroll_View = findViewById(R.id.scroll_View)
        ll_out = findViewById(R.id.ll_out)
        tv_set_ll_out_width = findViewById(R.id.tv_set_ll_out_width)
        tv_move_right = findViewById(R.id.tv_move_right)
        tv_move_left = findViewById(R.id.tv_move_left)
        tv_set_left = findViewById(R.id.tv_set_left)
        tv_set_middle = findViewById(R.id.tv_set_middle)
        tv_set_right = findViewById(R.id.tv_set_right)
        tv_air = findViewById(R.id.tv_air)
        tv_megre = findViewById(R.id.tv_megre)
        tv_stop = findViewById(R.id.tv_stop)
        tv_go_on = findViewById(R.id.tv_go_on)
        tv_move_middle = findViewById(R.id.tv_move_middle)
        //初始计算完整宽度
//        setContainerWidth()
//        for (index in 0..<allRows){
//            addView(index,10)
//        }

        initAddView()
        tv_set_left?.setOnClickListener {
            //初始计算完整宽度
//            setContainerWidth()
//            ll_out?.removeAllViews()
//            fixRow = 0
//            allBeans.clear()
//            for (index in 0..<allRows){
//                addView(index,10)
//            }
            fixRow = 0
            initAddView()
        }
        tv_set_middle?.setOnClickListener {
            //初始计算完整宽度
//            setContainerWidth()
//            ll_out?.removeAllViews()
//            fixRow = Random.nextInt(1, allRows) // 生成介于 1 和 100 之间的随机数
//            allBeans.clear()
//            for (index in 0..<allRows){
//                addView(index,10)
//            }
            fixRow = Random.nextInt(1, allRows)
            initAddView()
        }
        tv_set_right?.setOnClickListener {
            //初始计算完整宽度
//            setContainerWidth()
//            ll_out?.removeAllViews()
//            fixRow = allRows-1
//            allBeans.clear()
//            for (index in 0..<allRows){
//                addView(index,10)
//            }
            fixRow = allRows-1
            initAddView()
        }
        tv_move_right?.setOnClickListener {
            moveToRight(selectIndex)
        }
        tv_move_left?.setOnClickListener {
            moveToLeft(selectIndex)
        }
        tv_air?.setOnClickListener {
            air()
        }
        tv_megre?.setOnClickListener {
            merge()
        }
        tv_stop?.setOnClickListener {
            stop()
        }
        tv_go_on?.setOnClickListener {
            goOn()
        }
        tv_move_middle?.setOnClickListener {
            moveToMiddle(selectIndex)
        }
        tv_set_ll_out_width?.setOnClickListener {
            //动态设置控件宽度无效  ？》？？？？？？灵异
//            ll_out!!.layoutParams.width = 1000
//            ll_out!!.layoutParams.height = 1000
            val linearLayoutParams = LinearLayout.LayoutParams(ventilation_width, LinearLayout.LayoutParams.MATCH_PARENT)
            ll_out!!.layoutParams = linearLayoutParams
            Log.e("TAG","获取设置后的 ll_out宽度000 ${ll_out!!.width}")

//            val placeholderView = findViewById<View>(R.id.placeholder_view)
//            placeholderView.layoutParams.width = 1000
//            placeholderView.requestLayout() // 刷新布局
//            Log.e("TAG","获取设置后的 ll_out 宽度111 ${ll_out!!.width}")
        }


    }
    fun initAddView(){
        setContainerWidth()
        ll_out?.removeAllViews()
        allBeans.clear()
        for (index in 0..<allRows){
            addView(index,10)
        }


    }

    override fun onResume() {
        super.onResume()

    }
    private fun setContainerWidth() {
        var leftAndRight = dip2px(this,100f) // 这个是默认左右两侧的 预留宽度
        ventilation_width = 0
        ventilation_width+=leftAndRight
        for (index in 0..<allRows) {
            ventilation_width += 600 // 直接设置最大距离
        }
        Log.e("TAG","动态设置通风状态下的宽度：$ventilation_width")
        val linearLayoutParams = LinearLayout.LayoutParams(ventilation_width, LinearLayout.LayoutParams.MATCH_PARENT)
        ll_out!!.layoutParams = linearLayoutParams
        Log.e("TAG", "获取设置后的 ll_out宽度setContainerWidth ${ll_out!!.width}")
        if (fixRow==0){
            ll_out!!.gravity = Gravity.LEFT
        }else if (fixRow==allRows-1){
            ll_out!!.gravity = Gravity.RIGHT
        }else{
            ll_out!!.gravity = Gravity.CENTER_HORIZONTAL
        }
    }

    /**
     * 动态 add view
     */
    private fun addView(index: Int,margin:Int=10) {
        val linearLayout = LinearLayout(this) // Replace 'context' with your actual context
        val linearLayoutParams = LinearLayout.LayoutParams(300, LinearLayout.LayoutParams.MATCH_PARENT)
        linearLayoutParams.setMargins(margin, 10, margin, 10) // Set left, top, right, bottom margins for the linearLayout
        linearLayout.layoutParams = linearLayoutParams
        linearLayout.orientation = LinearLayout.VERTICAL
        linearLayout.gravity = Gravity.CENTER
        linearLayout.minimumHeight = 600 // 设置最小高度

        val textView = TextView(this) // Replace 'context' with your actual context
        textView.text = " $index"
        val textViewParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        textViewParams.gravity = Gravity.CENTER
        textViewParams.topMargin = 50
        textViewParams.bottomMargin = 50
        textView.layoutParams = textViewParams

        val imageView = ImageView(this)
        imageView.setBackgroundResource(R.mipmap.ic_launcher)
        if (index==fixRow){
            var imageViewParams = LinearLayout.LayoutParams(dip2px(this,60f), dip2px(this,60f))
            imageViewParams.topMargin = 50
            imageViewParams.bottomMargin = 50
            imageViewParams.gravity = Gravity.CENTER
            imageView.layoutParams = imageViewParams
        }else{
            var imageViewParams = LinearLayout.LayoutParams(dip2px(this,50f), dip2px(this,50f))
            imageViewParams.topMargin = 50
            imageViewParams.bottomMargin = 50
            imageViewParams.gravity = Gravity.CENTER
            imageView.layoutParams = imageViewParams
        }

        val textViewFixName = TextView(this) // Replace 'context' with your actual context
        textViewFixName.text = if (index==fixRow) "固定列" else ""
        val textViewFixNameParm = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        textViewFixNameParm.gravity = Gravity.CENTER
        textViewFixNameParm.topMargin = 50
        textViewFixNameParm.bottomMargin = 50
        textViewFixName.layoutParams = textViewFixNameParm

        linearLayout.addView(textView)
        linearLayout.addView(imageView)
        linearLayout.addView(textViewFixName)
        if (index==fixRow){
            linearLayout.setBackgroundColor(Color.parseColor("#458721"))
        }else{
            linearLayout.setBackgroundColor(Color.parseColor("#bb996c"))
        }
        ll_out?.addView(linearLayout)
        allAnimation.add(null) // 每一个view 都有自己一个对应的 animation

        linearLayout.viewTreeObserver.addOnGlobalLayoutListener (object : ViewTreeObserver.OnGlobalLayoutListener{
            override fun onGlobalLayout() {
                linearLayout.viewTreeObserver.removeOnGlobalLayoutListener(this)
                var rowBean = RowBean()
                rowBean.rawX = linearLayout.left.toFloat()
                rowBean.currentX = linearLayout.left.toFloat()
                rowBean.name = "$index"
                if (index<fixRow){
                    rowBean.destX = linearLayout.left.toFloat()-rowBean.moveDistance
                    rowBean.middleX = linearLayout.left.toFloat()-(rowBean.moveDistance/2)
                    rowBean.airDistance = rowBean.middleX*index
                }else if (index==fixRow){
                    rowBean.destX = linearLayout.left.toFloat()
                    rowBean.middleX = linearLayout.left.toFloat()
                    rowBean.airDistance = rowBean.middleX*index
                }else if (index>fixRow){
                    rowBean.destX = linearLayout.left.toFloat()+rowBean.moveDistance
                    rowBean.middleX = linearLayout.left.toFloat()+(rowBean.moveDistance/2)
                    rowBean.airDistance = rowBean.middleX*index
                }
                if (index==0){
                    ventilation_width = 0
                }
                ventilation_width+=(linearLayout.width+100)
                allBeans.add(rowBean)
                if (rowBean.rawX==rowBean.destX){
                    scroll_View?.scrollX = rowBean.rawX.toInt()
                }
                if (index==allRows-1){
                    original_width = ll_out?.width!!
                    Log.e("TAG","原始宽度：=$original_width")
                    Log.e("TAG","获取设置后的 ll_out宽度addView  ${ll_out!!.width}")

                    scroll_View!!.viewTreeObserver.addOnScrollChangedListener {
                        val scrollX = scroll_View!!.scrollX
                        var viewLeft = ll_out!!.getChildAt(0).left
                        var viewY = ll_out!!.getChildAt(0).y
                        var viewRight = ll_out!!.getChildAt(allRows-1).right
                        // 当滚动到固定位置时，回弹
                        if (scrollX >= viewRight - (5*300) ) {
                            scroll_View!!.smoothScrollTo(viewRight-(5*300),viewY.toInt())
                        }else if (scrollX <= viewLeft - 300){
                            scroll_View!!.smoothScrollTo(viewLeft,viewY.toInt())
                        } else {
                            // 允许滚动
//                            scroll_View!!.setOnTouchListener(null)
                        }
                    }

                    scroll_View!!.setOnTouchListener { _, event ->
                        when (event.action) {
                            MotionEvent.ACTION_DOWN -> {
                            }
                            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                                val scrollX = scroll_View!!.scrollX
                                val viewLeft = ll_out!!.getChildAt(0).left
                                val viewY = ll_out!!.getChildAt(0).y
                                val viewRight = ll_out!!.getChildAt(allRows - 1).right
                                // 当滚动到固定位置时，禁止滚动
                                if (scrollX >= viewRight + 100) {
                                    scroll_View!!.smoothScrollTo(viewRight - (5 * 300), viewY.toInt())
                                } else if (scrollX <= viewLeft - 300) {
                                    scroll_View!!.smoothScrollTo(viewLeft, viewY.toInt())
                                }
                            }
                        }
                        false
                    }
                }
            }

        })


        for (index in 0..(ll_out?.childCount?.minus(1) ?: 1)){
            var view = ll_out?.getChildAt(index)

            view?.setOnClickListener {v->
                val clickedIndex = ll_out?.indexOfChild(v)
                for (index in 0 until ll_out?.childCount!!) {
                    val childView = ll_out?.getChildAt(index)
                    if (index != fixRow) {
                        childView?.setBackgroundColor(Color.parseColor("#bb996c"))
                    }
                }
                selectIndex = clickedIndex!!
                v.setBackgroundColor(Color.parseColor("#FF3700B3"))
                Log.e("TAG","view 当前 x 坐标${v.x} 对象坐标：${allBeans[index]} 选择的索引 $selectIndex")
            }
        }
    }
    /**
     * 移动到中间 收到硬件给的移动指令 直接先移动到中间位置 ，硬件移动结束后 根据硬件 给的移动方向 执行 moveToLeft  或者 moveToRight
     */
    fun moveToMiddle(clickedIndex: Int){ // 不确定实际场景是否需要推动 左侧或右侧进行移动
        if (isAir){
            Log.e("TAG","正在通风不可移动")
            Toast.makeText(this,"正在通风不可移动",Toast.LENGTH_SHORT).show()
            return
        }
        if (clickedIndex==fixRow){
            Log.e("TAG","固定列不可移动")
            Toast.makeText(this,"固定列不可移动",Toast.LENGTH_SHORT).show()
            return
        }
        var delay:Long = 0
        var rowBean = allBeans[clickedIndex]
        if (rowBean.currentX!=rowBean.middleX){
            if (clickedIndex<fixRow){ // 点击位置在固定列左侧
                //向中间移动 需要先判断其左侧 从哪里开始移动
                if (rowBean.currentX<rowBean.middleX){ // 如果点击位置在固定列左侧 且 当前坐标 < 中间位置坐标 那么说明其向左移动过
                    var endIndex = clickedIndex // 移动过的view 结束 索引位置 倒叙 获取到第一个移动过的即结束
                    for (i in fixRow-1 downTo clickedIndex){
                        var currentRowBean = allBeans[i]
                        if (currentRowBean.currentX<currentRowBean.middleX){
                            endIndex = i
                            break
                        }
                    }
                    for (i in endIndex downTo clickedIndex){
                        var currentView = ll_out?.getChildAt(i)
                        var currentRowBean = allBeans[i]
                        allAnimation[i] = ValueAnimator.ofFloat(currentRowBean.currentX, currentRowBean.middleX)
                        allAnimation[i]?.startDelay = delay
                        allAnimation[i]?.duration = moveTime
                        allAnimation[i]?.addUpdateListener { animation ->
                            val currentValue = animation.animatedValue as Float
                            // 更新视图的左边距，实现平移动画
                            currentView?.layout(currentValue.toInt(), currentView.top, (currentValue + currentView.width).toInt(), currentView.bottom)
                            // 更新当前 x 坐标
                            currentRowBean.currentX = currentView!!.left.toFloat()
                            Log.e("TAG", "实时更新获取当前 view 对象 x 坐标：${currentRowBean.currentX}    view 平移距离：${currentView!!.left}")
                        }
                        allAnimation[i]?.start()
                        delay+=delayAddTime
                    }
                }else{ // 否则则计算从左侧开始到点击位置
                    var beginIndex = 0
                    for (i in 0..clickedIndex){
                        var currentRowBean = allBeans[i]
                        if (currentRowBean.currentX>currentRowBean.middleX){
                            beginIndex = i
                            break
                        }
                    }
                    for (i in beginIndex..clickedIndex){
                        var currentView = ll_out?.getChildAt(i)
                        var currentRowBean = allBeans[i]
                        allAnimation[i] = ValueAnimator.ofFloat(currentRowBean.currentX, currentRowBean.middleX)
                        allAnimation[i]?.startDelay = delay
                        allAnimation[i]?.duration = moveTime
                        allAnimation[i]?.addUpdateListener { animation ->
                            val currentValue = animation.animatedValue as Float
                            // 更新视图的左边距，实现平移动画
                            currentView?.layout(currentValue.toInt(), currentView.top, (currentValue + currentView.width).toInt(), currentView.bottom)
                            // 更新当前 x 坐标
                            currentRowBean.currentX = currentView!!.left.toFloat()
                            Log.e("TAG", "实时更新获取当前 view 对象 x 坐标：${currentRowBean.currentX}    view 平移距离：${currentView!!.left}")
                        }
                        allAnimation[i]?.start()
                        delay+=delayAddTime
                    }
                }

            }else if (clickedIndex>fixRow){ //  固定列在点击位置左侧
                if (rowBean.currentX<rowBean.middleX){ // 说明当前点击view依旧在其中间位置左侧，触发中间移动时候判断其右侧是否有处于初始坐标的view
                    var beginIndex = clickedIndex // 默认移动自身
                    for (i in allRows-1 downTo clickedIndex){ // 降序判断从哪一个view先开始移动 条件为 当前view已经移动到>中间位置的
                        var currentRowBean = allBeans[i]
                        if (currentRowBean.currentX<currentRowBean.middleX){
                            beginIndex = i
                            break
                        }
                    }
                    for (i in beginIndex downTo clickedIndex){
                        var currentView = ll_out?.getChildAt(i)
                        var currentRowBean = allBeans[i]
                        allAnimation[i] = ValueAnimator.ofFloat(currentRowBean.currentX, currentRowBean.middleX)
                        allAnimation[i]?.startDelay = delay
                        allAnimation[i]?.duration = moveTime
                        allAnimation[i]?.addUpdateListener { animation ->
                            val currentValue = animation.animatedValue as Float
                            // 更新视图的左边距，实现平移动画
                            currentView?.layout(currentValue.toInt(), currentView.top, (currentValue + currentView.width).toInt(), currentView.bottom)
                            // 更新当前 x 坐标
                            currentRowBean.currentX = currentView!!.left.toFloat()
                            Log.e("TAG", "实时更新获取当前 view 对象 x 坐标：${currentRowBean.currentX}    view 平移距离：${currentView!!.left}")
                        }
                        allAnimation[i]?.start()
                        delay+=delayAddTime
                    }
                }else if (rowBean.currentX>=rowBean.middleX){ //说明当前点击view 移动已经超过中间位置 再次移动的情况下 该view 应该向左移动
                    //向左移动 则需判断其左侧view情况
                    var beginIndex = clickedIndex // 默认移动自身
                    for (i in fixRow..clickedIndex){
                        var currentRowBean = allBeans[i]
                        if (currentRowBean.currentX>currentRowBean.middleX){
                            beginIndex = i
                            break
                        }
                    }
                    for (i in beginIndex..clickedIndex){
                        var currentView = ll_out?.getChildAt(i)
                        var currentRowBean = allBeans[i]
                        allAnimation[i] = ValueAnimator.ofFloat(currentRowBean.currentX, currentRowBean.middleX)
                        allAnimation[i]?.startDelay = delay
                        allAnimation[i]?.duration = moveTime
                        allAnimation[i]?.addUpdateListener { animation ->
                            val currentValue = animation.animatedValue as Float
                            // 更新视图的左边距，实现平移动画
                            currentView?.layout(currentValue.toInt(), currentView.top, (currentValue + currentView.width).toInt(), currentView.bottom)
                            // 更新当前 x 坐标
                            currentRowBean.currentX = currentView!!.left.toFloat()
                            Log.e("TAG", "实时更新获取当前 view 对象 x 坐标：${currentRowBean.currentX}    view 平移距离：${currentView!!.left}")
                        }
                        allAnimation[i]?.start()
                        delay+=delayAddTime
                    }
                }
            }
        }
    }


    /**
     * 开始移动前取消所有正在执行和未执行的动画 ,然后再执行新的动画
     */
    private fun cancleAllAnimation() {
        for (i in 0..<allAnimation.size) {
            allAnimation[i]?.let {
                it.cancel()
            }
        }
    }
    /**
     * 向右移动
     */
    fun moveToRight(clickedIndex: Int){
        if (isAir){
            Log.e("TAG","正在通风不可移动")
            Toast.makeText(this,"正在通风不可移动",Toast.LENGTH_SHORT).show()
            return
        }
        if (clickedIndex==fixRow){
            Log.e("TAG","固定列不可移动")
            Toast.makeText(this,"固定列不可移动",Toast.LENGTH_SHORT).show()
            return
        }
        val childCount = ll_out?.childCount ?: 0
        if (fixRow==0){ // 固定列在最左侧
            var delay: Long = 0 // 初始延迟时间
            cancleAllAnimation()
            for (i in childCount-1 downTo clickedIndex){
                var view = ll_out?.getChildAt(i)
                var rowBean = allBeans[i]
                var viewx = view!!.x
                if (viewx==rowBean.destX){ //说明已经移动到位
                    continue
                }
                allAnimation[i] = ValueAnimator.ofFloat(rowBean.currentX, rowBean.destX)
                allAnimation[i]?.startDelay = delay
                allAnimation[i]?.duration = moveTime
                allAnimation[i]?.addUpdateListener { animation ->
                    val currentValue = animation.animatedValue as Float
                    // 更新视图的左边距，实现平移动画
                    view?.layout(currentValue.toInt(), view.top, (currentValue + view.width).toInt(), view.bottom)
                    // 更新当前 x 坐标
                    rowBean.currentX = view.left.toFloat()
                    Log.e("TAG", "实时更新获取当前 view 对象 x 坐标：${rowBean.currentX}    view 平移距离：${view.left}")
                }
                allAnimation[i]?.start()
                delay+=delayAddTime
            }
        }else if (fixRow==childCount-1){ // 固定列在最右侧
            var delay: Long = 0 // 初始延迟时间
            var beginIndex = -1
            for (i in fixRow downTo clickedIndex){
                var rowBean = allBeans[i]
                if (rowBean.currentX==rowBean.destX||rowBean.currentX==rowBean.middleX||rowBean.currentX!=rowBean.rawX){
                    beginIndex = i
                    break
                }
            }
            cancleAllAnimation()
            for (i in beginIndex downTo clickedIndex){
                var view = ll_out?.getChildAt(i)
                var rowBean = allBeans[i]
                var viewx = view!!.x
                if (viewx==rowBean.rawX){ //说明已经移回原位
                    continue
                }
                allAnimation[i] = ValueAnimator.ofInt(rowBean.currentX.toInt(), rowBean.rawX.toInt())
                allAnimation[i]?.startDelay=delay
                allAnimation[i]?.duration = moveTime
                allAnimation[i]?.addUpdateListener { animation ->
                    val currentValue = animation.animatedValue as Int
                    // 更新视图的左边距，实现平移动画
                    view?.layout(currentValue, view.top, currentValue + view.width, view.bottom)
                    // 更新当前 x 坐标
                    rowBean.currentX = view.left.toFloat()
                    Log.e("TAG", "固定列在右侧 向左移动实时更新获取当前 view 对象 x 坐标：${rowBean.currentX}    view 左边距 ${view.left}")
                }
                allAnimation[i]?.start()
                delay+=delayAddTime
            }
        }else{ //固定列在中间
            if (clickedIndex<fixRow){
                var delay: Long = 0 // 初始延迟时间
//                moveBeforeCancle()
                var beginIndex = fixRow
                for (i in fixRow downTo clickedIndex){
                    var rowBean = allBeans[i]
                    if (rowBean.rawX==rowBean.destX){
                        continue
                    }
                    if (rowBean.currentX==rowBean.destX||rowBean.currentX==rowBean.middleX||rowBean.currentX!=rowBean.rawX){
                        beginIndex=i
                        break
                    }
                }
                cancleAllAnimation()
                for (i in beginIndex downTo clickedIndex){
                    var view = ll_out?.getChildAt(i)
                    var rowBean = allBeans[i]
                    var viewx = view!!.x
                    if (viewx==rowBean.rawX){ //说明已经移回原位
                        continue
                    }
                    allAnimation[i] = ValueAnimator.ofInt(rowBean.currentX.toInt(), rowBean.rawX.toInt())
                    allAnimation[i]?.startDelay=delay
                    allAnimation[i]?.duration = moveTime
                    allAnimation[i]?.addUpdateListener { animation ->
                        val currentValue = animation.animatedValue as Int
                        // 更新视图的左边距，实现平移动画
                        view?.layout(currentValue, view.top, currentValue + view.width, view.bottom)
                        // 更新当前 x 坐标
                        rowBean.currentX = view.left.toFloat()
                        Log.e("TAG", "固定列在右侧 向左移动实时更新获取当前 view 对象 x 坐标：${rowBean.currentX}    view 左边距 ${view.left}")
                    }
                    allAnimation[i]?.start()
                    delay+=delayAddTime
                }
            }else{
                var delay: Long = 0 // 初始延迟时间
                cancleAllAnimation()
                for (i in childCount-1 downTo clickedIndex){
                    var view = ll_out?.getChildAt(i)
                    var rowBean = allBeans[i]
                    var viewx = view!!.x
                    if (viewx==rowBean.destX){ //说明已经移动到位
                        continue
                    }
                    allAnimation[i] = ValueAnimator.ofFloat(rowBean.currentX, rowBean.destX)
                    allAnimation[i]?.startDelay = delay
                    allAnimation[i]?.duration = moveTime
                    allAnimation[i]?.addUpdateListener { animation ->
                        val currentValue = animation.animatedValue as Float
                        // 更新视图的左边距，实现平移动画
                        view?.layout(currentValue.toInt(), view.top, (currentValue + view.width).toInt(), view.bottom)
                        // 更新当前 x 坐标
                        rowBean.currentX = view.left.toFloat()
                        Log.e("TAG", "实时更新获取当前 view 对象 x 坐标：${rowBean.currentX}    view 平移距离：${view.left}")
                    }
                    allAnimation[i]?.start()
                    delay+=delayAddTime
                }
            }
        }
    }


    /**
     * 向左移动
     */
    fun moveToLeft(clickedIndex: Int){
        if (isAir){
            Log.e("TAG","正在通风不可移动")
            Toast.makeText(this,"正在通风不可移动",Toast.LENGTH_SHORT).show()
            return
        }
        if (clickedIndex==fixRow){
            Log.e("TAG","固定列不可移动")
            Toast.makeText(this,"固定列不可移动",Toast.LENGTH_SHORT).show()
            return
        }
        val childCount = ll_out?.childCount ?: 0
        if (fixRow==0) { // 固定列在最左侧
            var beginIndex = -1
            for (i in 1..<childCount){
                var view = ll_out?.getChildAt(i)
                var rowBean = allBeans[i]
                if (rowBean.currentX==rowBean.destX||rowBean.currentX==rowBean.middleX||rowBean.currentX!=rowBean.rawX){ //说明已经移动到位
                    beginIndex = i
                    break
                }
            }
            if (beginIndex==-1){
                Log.e("TAG","没有向右移动过不可向左移动")
                return
            }
            var delay: Long = 0 // 初始延迟时间
            cancleAllAnimation()
            for (i in beginIndex..clickedIndex){
                var view = ll_out?.getChildAt(i)
                var rowBean = allBeans[i]
                var viewx = view!!.x
                if (viewx==rowBean.rawX){ //说明已经移回原位
                    continue
                }
                allAnimation[i] = ValueAnimator.ofInt(rowBean.currentX.toInt(), rowBean.rawX.toInt())
                allAnimation[i]?.startDelay = delay
                allAnimation[i]?.duration = moveTime
                allAnimation[i]?.addUpdateListener { animation ->
                    val currentValue = animation.animatedValue as Int
                    // 更新视图的左边距，实现平移动画
                    view?.layout(currentValue.toInt(), view.top, (currentValue + view.width).toInt(), view.bottom)
                    // 更新当前 x 坐标
                    rowBean.currentX = view.left.toFloat()
                    Log.e("TAG", "固定列在最左侧 向左移动 实时更新获取当前 view 对象 x 坐标：${rowBean.currentX}    view 左边距 ${view.left}")
                }
                allAnimation[i]?.start()
                delay+=delayAddTime
            }
        }else if (fixRow==childCount-1){ // 固定列在最右侧
            var delay: Long = 0 // 初始延迟时间
            cancleAllAnimation()
            for (i in 0..clickedIndex){
                var view = ll_out?.getChildAt(i)
                var rowBean = allBeans[i]
                var viewx = view!!.left.toFloat()
                rowBean.currentX = viewx
                if (rowBean.currentX==rowBean.destX){ //说明已经移动到位
                    continue
                }
                allAnimation[i] = ValueAnimator.ofInt(rowBean.currentX.toInt(), rowBean.destX.toInt())
                allAnimation[i]?.startDelay = delay
                allAnimation[i]?.duration = moveTime
                allAnimation[i]?.addUpdateListener { animation ->
                    val currentValue = animation.animatedValue as Int
                    // 更新视图的左边距，实现平移动画
                    view?.layout(currentValue, view.top, currentValue + view.width, view.bottom)
                    // 更新当前 x 坐标
                    rowBean.currentX = view.left.toFloat()
                    Log.e("TAG", "固定列在右侧 向左移动 实时更新获取当前 view 对象 x 坐标：${rowBean.currentX}    view 左边距 ${view.left}")
                }
                allAnimation[i]?.start()
                delay+=delayAddTime
            }

        }else{ //固定列在中间
            if (clickedIndex<fixRow){ // 点击位置处于固定列左侧 初始只能向左移动 不可向右移动
                var delay: Long = 0 // 初始延迟时间
                var beginIndex = 0
                for (i in fixRow downTo clickedIndex){
                    var rowBean = allBeans[i]
                    if (rowBean.rawX==rowBean.destX){
                        continue
                    }
                    if (rowBean.currentX==rowBean.destX||rowBean.currentX==rowBean.middleX||rowBean.currentX!=rowBean.rawX){
                        beginIndex=i
                        break
                    }
                }
                for (i in beginIndex..clickedIndex){
                    var view = ll_out?.getChildAt(i)
                    var rowBean = allBeans[i]
                    var viewx = view!!.left.toFloat()
                    rowBean.currentX = viewx
                     if (rowBean.currentX==rowBean.destX){ //说明已经移动到位 向左移动 已经移动到目标位置 不可再次向左移动
                        continue
                    }
                    allAnimation[i] = ValueAnimator.ofInt(rowBean.currentX.toInt(), rowBean.destX.toInt())
                    allAnimation[i]?.startDelay = delay
                    allAnimation[i]?.duration = moveTime
                    allAnimation[i]?.addUpdateListener { animation ->
                        val currentValue = animation.animatedValue as Int
                        // 更新视图的左边距，实现平移动画
                        view?.layout(currentValue, view.top, currentValue + view.width, view.bottom)
                        // 更新当前 x 坐标
                        rowBean.currentX = view.left.toFloat()
                        Log.e("TAG", "固定列在右侧 向左移动 实时更新获取当前 view 对象 x 坐标：${rowBean.currentX}    view 左边距 ${view.left}")
                    }
                    allAnimation[i]?.start()
                    delay+=delayAddTime
                }
            }else{
                var delay: Long = 0 // 初始延迟时间
                var beginIndex = -1
                for (i in fixRow..clickedIndex){
                    var rowBean = allBeans[i]
                    if (rowBean.rawX==rowBean.destX){
                        continue
                    }
                    if (rowBean.currentX==rowBean.destX||rowBean.currentX==rowBean.middleX||rowBean.currentX!=rowBean.rawX){
                        beginIndex = i
                        break
                    }
                }
                cancleAllAnimation()
                for (i in beginIndex .. clickedIndex){ // 固定列在中间 从当前点击位置 降序 固定列位置 判断其是否能左移
                    var view = ll_out?.getChildAt(i)
                    var rowBean = allBeans[i]
                    var viewx = view!!.x
                    if (viewx == rowBean.rawX) { //说明目前在原位置 不可向左移动
                        continue
                    }
                    allAnimation[i] = ValueAnimator.ofFloat(rowBean.currentX, rowBean.rawX) // 向左移动 移动回初始位置
                    allAnimation[i]?.startDelay = delay
                    allAnimation[i]?.duration = moveTime
                    allAnimation[i]?.addUpdateListener { animation -> val currentValue = animation.animatedValue as Float
                        // 更新视图的左边距，实现平移动画
                        view?.layout(currentValue.toInt(), view.top, (currentValue + view.width).toInt(), view.bottom)
                        // 更新当前 x 坐标
                        rowBean.currentX = view.left.toFloat()
                        Log.e("TAG", "实时更新获取当前 view 对象 x 坐标：${rowBean.currentX}    view 平移距离：${view.left}")
                    }
                    allAnimation[i]?.start()
//                    allAnimation[i] = animator
                    delay+=delayAddTime
                }
            }
        }
    }
    fun pxToDp(px: Int, context: Context): Int {
        val density = context.resources.displayMetrics.density
        return (px / density).toInt()
    }
    private fun dip2px(context: Context, dipValue: Float): Int {
        val r = context.resources
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, dipValue, r.displayMetrics
        ).toInt()
    }
    /**
     * 通风
     */
    fun air(){
        isAir = true
        for (i in 0 until allRows){
            var view = ll_out?.getChildAt(i)
            var rowBean = allBeans[i]
            if (rowBean.rawX==rowBean.destX){
                continue
            }
            if (fixRow==0){// 固定列在最左侧
                allAnimation[i] = ValueAnimator.ofFloat(rowBean.currentX, rowBean.rawX+(rowBean.middleX-rowBean.rawX)*i) // 通风全部从当前位置 移动到目标位置
            }else if (fixRow==allRows-1){ // 固定列在最右侧
                allAnimation[i] = ValueAnimator.ofFloat(rowBean.currentX, rowBean.rawX-(rowBean.rawX-rowBean.middleX)*(allRows-1-i) ) // 通风全部从当前位置 移动到目标位置
            }else{
                if (i < fixRow) {  // 固定类在中间 其左侧视图 （即固定列在右侧）
                    allAnimation[i] = ValueAnimator.ofFloat(rowBean.currentX, rowBean.rawX-(rowBean.rawX-rowBean.middleX)*(fixRow-i) ) // 通风全部从当前位置 移动到目标位置
                } else if (i>fixRow) {   // 固定类在中间 其右侧侧视图 （即固定列在左侧）
                    allAnimation[i] = ValueAnimator.ofFloat(rowBean.currentX, rowBean.rawX+(rowBean.middleX-rowBean.rawX)*(i-fixRow)) // 通风全部从当前位置 移动到目标位置
                }
            }
            allAnimation[i]?.duration = moveTime
            allAnimation[i]?.addUpdateListener { animation -> val currentValue = animation.animatedValue as Float
                // 更新视图的左边距，实现平移动画
                view?.layout(currentValue.toInt(), view.top, (currentValue + view.width).toInt(), view.bottom)
                // 更新当前 x 坐标
                rowBean.currentX = view!!.left.toFloat()
                Log.e("TAG", "通风 view 对象 x 坐标：原始坐标${rowBean.rawX} 移动后坐标${rowBean.currentX}    view 平移距离：${view.left}")
            }
            allAnimation[i]?.addListener(object :AnimatorListener{
                override fun onAnimationStart(animation: Animator) {

                }

                override fun onAnimationEnd(animation: Animator) {
                    var viewxy = ll_out!!.getChildAt(fixRow)
                    scroll_View?.smoothScrollTo(viewxy.x.toInt(),viewxy.y.toInt())
                }

                override fun onAnimationCancel(animation: Animator) {
                }

                override fun onAnimationRepeat(animation: Animator) {
                }
            })
            allAnimation[i]?.start()
        }
    }
    /**
     * 合架
     */
    fun merge(){
        isAir = false
        for (i in 0..<allRows){
            var view = ll_out?.getChildAt(i)
            var rowBean = allBeans[i]
            allAnimation[i] = ValueAnimator.ofFloat(rowBean.currentX, rowBean.rawX) // 合架全部从当前位置 移动到初始位置
            allAnimation[i]?.duration = moveTime
            allAnimation[i]?.addUpdateListener { animation -> val currentValue = animation.animatedValue as Float
                // 更新视图的左边距，实现平移动画
                view?.layout(currentValue.toInt(), view.top, (currentValue + view.width).toInt(), view.bottom)
                // 更新当前 x 坐标
                rowBean.currentX = view!!.left.toFloat()
                Log.e("TAG", "实时更新获取当前 view 对象 x 坐标：${rowBean.currentX}    view 平移距离：${view.left}")
            }
            allAnimation[i]?.addListener(object :AnimatorListener{
                override fun onAnimationStart(animation: Animator) {

                }

                override fun onAnimationEnd(animation: Animator) {
                    var viewxy = ll_out!!.getChildAt(fixRow)
                    scroll_View?.smoothScrollTo(viewxy.x.toInt(),viewxy.y.toInt())
                }

                override fun onAnimationCancel(animation: Animator) {
                }

                override fun onAnimationRepeat(animation: Animator) {
                }
            })
            allAnimation[i]?.start()
        }
    }
    /**
     * 停止
     */
    fun stop(){
        try {
            for (i in 0..<allAnimation.size){
                var animation = allAnimation[i]
                animation?.let {
                    it.pause()
                }
            }
        } catch (e: Exception) {
            Log.e("TAG","处理正在移动中的动画停止,因初始为null，直接全部可能发生异常")
        }
    }

    /**
     * 继续移动
     */
    fun goOn(){
        try {
            for (i in 0..<allAnimation.size){
                var animation = allAnimation[i]
                animation?.let {
                    it.resume()
                }
            }
        } catch (e: Exception) {
            Log.e("TAG","处理正在停止的动画继续执行,因初始为null，直接全部可能发生异常")
        }
    }
}